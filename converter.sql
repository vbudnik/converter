-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3307
-- Час створення: Січ 25 2019 р., 11:57
-- Версія сервера: 5.7.23
-- Версія PHP: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `converter`
--

-- --------------------------------------------------------

--
-- Структура таблиці `converter`
--

CREATE TABLE `converter` (
  `id` int(11) NOT NULL,
  `operations` text NOT NULL,
  `current_value` double NOT NULL,
  `final_value` double NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `converter`
--

INSERT INTO `converter` (`id`, `operations`, `current_value`, `final_value`, `date`) VALUES
(124, 'USD -> UAH', 1, 27.5, '2019-01-25 19:46:01'),
(123, 'USD -> UAH', 10, 275, '2019-01-25 19:45:56'),
(122, 'USD -> UAH', 4.509091, 124.0000025, '2019-01-25 19:45:52'),
(121, 'USD -> UAH', 4.509091, 124.0000025, '2019-01-25 19:45:29'),
(120, 'UAH -> USD', 124, 4.5090909090909, '2019-01-25 19:45:20'),
(119, 'USD -> UAH', 12, 330, '2019-01-25 19:45:07'),
(118, 'USD -> UAH', 1, 27.5, '2019-01-25 19:44:29'),
(117, 'USD -> UAH', 1, 27.5, '2019-01-25 19:44:07'),
(116, 'USD -> UAH', 1, 27.5, '2019-01-25 19:43:25'),
(115, 'BTC -> USD', 1, 3389.0015, '2019-01-25 19:43:13'),
(114, 'BTC -> USD', 12, 40716.8472, '2019-01-25 18:27:01'),
(113, 'UAH -> USD', 21, 0.76363636363636, '2019-01-25 18:26:56'),
(112, 'UAH -> USD', 3, 0.10909090909091, '2019-01-25 17:37:05'),
(111, 'UAH -> USD', 3, 0.10909090909091, '2019-01-25 17:37:03'),
(110, 'UAH -> USD', 12, 0.43636363636364, '2019-01-25 17:37:01'),
(109, 'UAH -> USD', 12, 0.43636363636364, '2019-01-25 17:37:00'),
(108, 'UAH -> USD', 21.3, 0.77454545454545, '2019-01-25 17:25:00'),
(107, 'UAH -> USD', 1, 0.036363636363636, '2019-01-25 17:24:57'),
(106, 'UAH -> USD', 12, 0.43636363636364, '2019-01-25 17:24:53'),
(105, 'UAH -> USD', 12, 0.43636363636364, '2019-01-25 17:23:47'),
(104, 'UAH -> USD', 12, 0.43636363636364, '2019-01-25 17:23:44');

-- --------------------------------------------------------

--
-- Структура таблиці `currency_pairs`
--

CREATE TABLE `currency_pairs` (
  `currency_pairs` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `route` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `currency_pairs`
--

INSERT INTO `currency_pairs` (`currency_pairs`, `status`, `route`) VALUES
('USD -> UAH', 1, 'usd-uah'),
('UAH -> USD', 0, 'uah-usd'),
('EUR -> UAH', 0, 'eur-uah'),
('UAH -> EUR', 0, 'uah-eur'),
('UAH -> RUR', 0, 'uah-rur'),
('RUR -> UAH', 0, 'rur-uah'),
('BTC -> USD', 0, 'btc-usd'),
('USD -> BTC', 0, 'usd-btc');

-- --------------------------------------------------------

--
-- Структура таблиці `status_user`
--

CREATE TABLE `status_user` (
  `user_id` int(11) NOT NULL,
  `stat` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `status_user`
--

INSERT INTO `status_user` (`user_id`, `stat`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 1),
(5, 0);

-- --------------------------------------------------------

--
-- Структура таблиці `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user`
--

INSERT INTO `user` (`id`, `login`, `email`, `password`) VALUES
(1, 'guest', 'none', '1'),
(2, 'budnik', 'budnik@gmail.com', '11111111'),
(3, 'budnik', 'vasyl@gmailc.om', '11111111'),
(4, 'admin', 'admin', '123'),
(5, 'new', 'new@mail.com', '11111111'),
(6, 'lol', 'lol@mail.com', '11111111'),
(7, 'lol', '123', '123'),
(8, 'vasyl', 'login@gmail.com', '11111111'),
(9, 'abcdfghfh', 'zwxy@gmail.com', '123'),
(10, 'zwxt', 'abcdef', '123'),
(11, 'zwxt', 'loa1696@gmail.com', '123');

-- --------------------------------------------------------

--
-- Структура таблиці `user_converter`
--

CREATE TABLE `user_converter` (
  `id_operation` int(11) NOT NULL,
  `id_user` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `user_converter`
--

INSERT INTO `user_converter` (`id_operation`, `id_user`) VALUES
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(74, 0),
(75, 5),
(76, 5),
(77, 0),
(78, 0),
(85, 1),
(80, 1),
(82, 4),
(83, 4),
(84, 4),
(87, 4),
(88, 4),
(89, 8),
(90, 1),
(91, 9),
(92, 9),
(93, 11),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 4),
(110, 4),
(111, 4),
(112, 4),
(113, 4),
(114, 4),
(115, 4),
(116, 4),
(117, 4),
(118, 4),
(119, 4),
(120, 4),
(121, 4),
(122, 4),
(123, 4),
(124, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `user_seting`
--

CREATE TABLE `user_seting` (
  `user_id` int(11) NOT NULL,
  `user_seting` varchar(255) NOT NULL,
  `max_element` int(11) NOT NULL,
  `type_sorting` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `converter`
--
ALTER TABLE `converter`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `converter`
--
ALTER TABLE `converter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT для таблиці `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
