<?php

return [

    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'usd-uah'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'uah-usd'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'eur-uah'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'uah-eur'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'uah-rur'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'rur-uah'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'btc-usd'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'usd-btc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    //account routs

    'account' => [
        'controller' => 'account',
        'action' => 'index',
    ],

    'account/{id:\d+}' => [
        'controller' => 'account',
        'action' => 'index',
    ],

    'registration' => [
        'controller' => 'account',
        'action' => 'registration',
    ],

    'login' => [
        'controller' => 'account',
        'action' => 'login',
    ],

    'logout' => [
        'controller' => 'account',
        'action' => 'logout',
    ],

    'seting' => [
        'controller' => 'account',
        'action' => 'seting',
    ],

    'view' => [
        'controller' => 'account',
        'action' => 'view',
    ],

];
