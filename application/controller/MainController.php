<?php

namespace application\controller;

use application\core\Controller;
use application\models\Converter;
use application\models\User;

class MainController extends Controller
{
    public function indexAction()
    {
        $finalValue = false;
        $error = false;

        if (isset($_SESSION['user'])) {
            $userId = User::getUserId();
            $userTypeConvert = Converter::getTypeConvert($userId);
            $jsonUserSeting = User::getUserSeting($userId);
            $userSeting = json_decode($jsonUserSeting['user_seting'], true);

            for($i = 0; $i < count($userSeting); $i++) {
                if($userSeting[$i]['route'] == $_SESSION['currencyExchange']) {
                     if ($userSeting[$i]['status'] == 0){
                         for($j = 0; $j < count($userSeting); $j++) {
                             if($userSeting[$j]['status'] == 1) {
                                 $_SESSION['currencyExchange'] = $userSeting[$j]['route'];
                                  break;
                             }
                         }
                     }
                     break;
                }
            }

        } else {
            $userId = 1;
        }

        if ($userId == 1){
            $currencyPairs = Converter::getCurrencyPairs();
        } else {
            $jsonCurrencyPairs = Converter::getCurrencyPairsUser($userId);
            $currencyPairs = json_decode($jsonCurrencyPairs['user_seting'], true);
        }

        if(!isset($_SESSION['currencyExchange'])) {;
            $_SESSION['currencyExchange'] = 'uah-usd';
        }

        $typeConvert = Converter::typeConvert($_SESSION['currencyExchange']);

        if ($userId != 1) {
            $res = mb_strtolower(str_replace(" -> ", "-", $userTypeConvert['type_convertyng']));
            Converter::setTypeConvert($userId, $typeConvert);
            $_SESSION['currencyExchange'] = $res;
        }

        if (isset($_POST['submit']) && isset($_POST['value'])) {
            $valueConvert = $_POST['value'];
            if (!is_numeric($valueConvert)){
                $error[] = "Only number";
            }
            if ($valueConvert < 0){
                $error[] = "Positive number";
            }
            if($error == false) {
                $currentValue = Converter::getCurrentValue($_SESSION['currencyExchange']);
                $currentCourse = Converter::getCurrentCourse($currentValue);
                $finalValue = Converter::getValueOfConvert($typeConvert, $currentCourse, $valueConvert);

                $convertId = Converter::addOperation($typeConvert, $finalValue, $valueConvert);
                if ($convertId){
                    Converter::setUserOperation($userId, $convertId);
                }
            }
            $this->view->redirect('/');
            return true;
        }

        $lastConverting = Converter::getLastConvertin();

        $vars = [
            'currencyPairs' => $currencyPairs,
            'lastConverting' => $lastConverting,
            'error' => $error,
            'typeSort' => $typeConvert,
            'value' => $finalValue,
        ];

        $this->view->render('CONVERTER | Main', $vars);
        return true;
    }


    public function sortAction() {
        if (!isset($_SESSION['currencyExchange'])) {
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $_SESSION['currencyExchange'] = $url;
            $this->view->redirect('/');
            return true;
        } else {
            $userId = User::getUserId();
            unset($_SESSION['currencyExchange']);
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $_SESSION['currencyExchange'] = $url;
            if(User::getUserId()) {
                Converter::setTypeConvert(User::getUserId(), $_SESSION['currencyExchange']);
            }
            $this->view->redirect('/');
            return true;
        }
    }
}
