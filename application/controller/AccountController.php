<?php

namespace application\controller;

use application\core\Controller;
use application\core\Pagination;
use application\models\Converter;
use application\models\User;


class AccountController extends Controller
{

    public function indexAction() {
        if (isset($_SESSION['user'])) {
            $userId = User::getUserId();
            $userMaxElement = User::getUserMaxElement($userId);

            if (!isset($this->route['id'])) {
                $route = 1;
            } else {
                $route = $this->route['id'];
            }

            $countId = Converter::countId($userId);

            $userOperatio = Converter::getOperationUser($userId, intval($userMaxElement['max_element']), $route);

            $pagination = new Pagination($this->route, $countId, $userMaxElement['max_element']);

            if (isset($this->route['id'])) {
                if ($this->route['id'] > ceil($countId / $userMaxElement['max_element'])) {
                    $this->view->errorCode(404);
                }
            }

            $vars = [
                'lastConverting' => $userOperatio,
                'pagination' => $pagination->get(),
            ];
            $this->view->render('Converter | Account List', $vars);
            return true;
        } else {
            $this->view->redirect('/login');
            return true;
        }
    }

    public function registrationAction()
    {
        if(User::checkLogin() == false){
            $errors = false;
            $vars = [
                'errors' => $errors,
            ];
             if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['psw'])
                && isset($_POST['pswrepeat']) && isset($_POST['name'])) {
                 $user['name'] = $_POST['name'];
                 $user['email'] = $_POST['email'];
                 $user['password'] = $_POST['psw'];
                 $user['pswrepeat'] = $_POST['pswrepeat'];

                 if($user['password'] != $user['pswrepeat']){
                     $errors[] = 'password does not match';
                 }
                 if (!User::checkEmail($user['email'])) {
                     $errors[] = 'Not correct email';
                 }
                 if (!User::checkPassword($user['password'])) {
                     $errors[] = 'Not correct password';
                 }
                 if ($errors == false) {
                     if (!User::checkUserData($user['email'], $user['password'])
                         && $user['password'] == $user['pswrepeat']) {
                         $userId = User::registerUser($user);
                         $currencyPairs = Converter::getCurrencyPairs();
                         $jsonCurrencyPairs = json_encode($currencyPairs, true);
                         User::addUserSeting($userId, $jsonCurrencyPairs);
                         User::auth($userId, $user['name']);
                         User::setStatus($userId);
                         $this->view->redirect('/');
                         return true;
                     }
                 } else {
                     $vars = [
                         'errors' => $errors,
                     ];
                     $this->view->render('Converter | LOGIN', $vars);
                     return true;
                 }
             }
            $this->view->render('Converter | Regiser', $vars);
            return true;
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

    public function loginAction() {
        if(User::checkLogin() == false) {
            $passwors = '';
            $email = '';
            $errors = false;
            $vars = [
                'errors' => null,
            ];


            if (isset($_POST['submit']) && isset($_POST['email']) && isset($_POST['password'])) {
                $passwors = $_POST['password'];
                $email = $_POST['email'];

                if ($email == 'admin' && $passwors == '123'){
                    $userId = User::checkUserData($email, $passwors);
                    $userData = User::checkExist($userId);
                    if($userData == false) {
                        $currencyPairs = Converter::getCurrencyPairs();
                        $jsonCurrencyPairs = json_encode($currencyPairs, true);
                        User::addUserSeting($userId, $jsonCurrencyPairs);
                    }
                    $userLogin = User::getUserName($userId);
                    User::auth($userId, $userLogin['login']);
                    $this->view->redirect('/');
                } else {
                    if (!User::checkEmail($email)) {
                        $errors[] = 'Not correct email';
                    }
                    if (!User::checkEmailExists($email)) {
                        $errors[] = 'email exists';
                    }
                    if (!User::checkPassword($passwors)) {
                        $errors[] = 'Not correct password';
                    }
                    if ($errors == false) {
                        $userId = User::checkUserData($email, $passwors);
                        $userData = User::checkExist($userId);
                        if($userData == false) {
                            $currencyPairs = Converter::getCurrencyPairs();
                            $jsonCurrencyPairs = json_encode($currencyPairs, true);
                            User::addUserSeting($userId, $jsonCurrencyPairs);
                        }
                        $userLogin = User::getUserName($userId);
                        User::auth($userId, $userLogin['login']);
                        $this->view->redirect('/');
                    } else {
                        if (isset($errors)) {
                            $vars = [
                                'errors' => $errors,
                            ];
                            $this->view->render('Converter | LOGIN', $vars);
                            return true;
                        }
                    }
                }
            }
            $this->view->render('Converter | LOGIN', $vars);
            return true;
        }
        $this->view->redirect('/account');
        return true;

    }

    public function logoutAction() {
        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            unset($_SESSION['name']);
            unset($_SESSION['currencyExchange']);
            $this->view->redirect('/');
            return true;
        }
        $this->view->redirect('/account');
        return true;
    }

    public function setingAction() {

        if (isset($_SESSION['user'])) {
            $userId = User::getUserId();
            $jsonCurrencyPairs = Converter::getCurrencyPairsUser($userId);

            $currencyPairs = json_decode($jsonCurrencyPairs['user_seting'], true);


            if (isset($currencyPairs['user_seting']) || $currencyPairs == null) {
                $currencyPairs = Converter::getCurrencyPairs();
            }

            $error = false;

            if (isset($_POST['submit'])) {
                if (!isset($_POST['check_list'])) {
                    $error[] = 'Check one position';
                    $vars = [
                        'currencyPairs' => $currencyPairs,
                        'errors' => $error,
                    ];
                    $this->view->render('Converter | Seting', $vars);
                    return true;
                } else {
                    $list = $_POST['check_list'];
                    for ($i = 0; $i < count($currencyPairs); $i++) {
                        if (!in_array($currencyPairs[$i]['currency_pairs'], $list)) {
                            $currencyPairs[$i]['status'] = 0;
                        } else {
                            $currencyPairs[$i]['status'] = 1;
                        }
                    }
                    $jsonCurrencyPairs = json_encode($currencyPairs, true);
                    $res = User::setUserSeting($userId, $jsonCurrencyPairs);

                    $this->view->redirect('/seting');
                }

            }
            $vars = [
                'currencyPairs' => $currencyPairs,
                'errors' => $error,
            ];


            $this->view->render('Converter | Seting', $vars);
            return true;
        } else {
            $this->view->redirect('/');
            return true;
        }
    }

    public function viewAction(){
        if (isset($_SESSION['user'])) {

            $error = false;
            if(isset($_POST['submit']) && isset($_POST['number'])) {
                $maxElement = $_POST['number'];
                if (!is_numeric($maxElement)){
                    $error[] = "Only number";
                }
                if ($maxElement < 0){
                    $error[] = "Positive number";
                }
                if ($error == false) {
                    $userId = User::getUserId();
                    $res = User::setMaxElemetToUser($maxElement, $userId);
                    if($res){
                        $this->view->redirect('/account');
                        return true;
                    }
                }
            }
            $vars = [
                'error' => $error,
            ];

            $this->view->render('Converter | View Seting', $vars);
            return true;
        } else {
            $this->view->redirect('/login');
            return true;
        }
    }

}