<h3>Register</h3>
<p>Please fill in this form to create an account.</p>
<hr>
<form action="" method="post">
    <?php if($vars['errors'] != false): ?>
        <?php foreach($vars['errors'] as $value): ?>
            <tr>
                <td><?php echo $value; ?></td>
                <br>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <label for="name">Name</label>
    <input type="text" placeholder="Enter Name" name="name" >
    <label for="email">Email</label>
    <input type="text" placeholder="Enter Email" name="email" >
    <label for="psw">Password</label>
    <input type="password" placeholder="Enter Password" name="psw" >
    <label for="psw-repeat">Repeat Password</label>
    <input type="password" placeholder="Repeat Password" name="pswrepeat" >
    <input type="submit" name="submit" value="Register"/>
</form>
