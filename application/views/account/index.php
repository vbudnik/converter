<section>
    <br>
    <h3>You operation list:</h3>
    <br>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">login</th>
            <th scope="col">E-mail</th>
            <th scope="col">Operation</th>
            <th scope="col">Status</th>
            <th scope="col">Current Value</th>
            <th scope="col">Final Value</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($vars['lastConverting'] as $task => $value): ?>
            <tr>
                <td scope="row"><?php echo $value['login']; ?></td>
                <td scope="row"><?php echo $value['email']; ?></td>
                <td scope="row"><?php echo $value['operations']; ?></td>
                <td scope="row"><?php echo $value['current_value']; ?></td>
                <td scope="row"><?php echo $value['final_value']; ?></td>
                <td scope="row"><?php echo $value['date']; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</section>
<section>
    <div class="clearfix">
        <?php echo $vars['pagination']; ?>
    </div>
</section>
