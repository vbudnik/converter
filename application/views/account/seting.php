<section>

    <?php if($vars['errors'] != false): ?>
        <b><p>ERROR</p></b>
        <?php foreach($vars['errors'] as $value): ?>
            <tr>
                <h5><td><?php echo $value; ?></td></h5>
                <br>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <br>

    <h3>Check Currency Pairs</h3>
    <form action="/seting" method="post">
        <?php foreach ($vars['currencyPairs'] as $key => $value): ?>
            <input type="checkbox" name="check_list[]" <?php if ($value['status'] == 1) echo ' checked';?> value="<?php echo $value['currency_pairs'];?>"><label> <?php echo $value['currency_pairs'];?></label><br/>
        <?php endforeach; ?>
        <br>
        <input type="submit" class="btn btn-primary" name="submit" value="Save"/>
    </form>
</section>