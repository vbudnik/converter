<h3>Input you data</h3>
<hr>
<form action="" method="post">
    <?php if(isset($vars['errors'])): ?>
        <?php foreach($vars['errors'] as $value): ?>
            <tr>
                <td><?php echo $value; ?></td>
                <br>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <br>
    <label for="email">Email</label>
    <input type="text" placeholder="Enter Email" name="email" >
    <label for="psw">Password</label>
    <input type="password" placeholder="Enter Password" name="password" >
    <input type="submit" name="submit" value="Login"/>
</form>
