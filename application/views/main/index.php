<section>
    <form action="#" method="post" >
        <label for="comment">Value to convert</label>
        <input type="textarea" name="value" placeholder="" value="">

        <input  class="btn btn-default" type="submit" name="submit" value="Convert">
        <br/><br/>
    </form>
</section>
<br>

<section>

    <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $vars['typeSort'];?>
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <?php foreach ($vars['currencyPairs'] as $key => $value): ?>
                <?php if ($value['status'] == 1):?>
                    <li><a href="/<?php echo $value['route'];?>"><?php echo $value['currency_pairs'];?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <br>
</section>
<section>
    <?php if ($vars['error'] != false): ?>
        <p><?php echo $vars['error'][0]; ?></p>
    <?php else:?>
        <?php if($vars['value'] != false): ?>
            <p>Current value: <b><?php printf("%.04f",$vars['value']);?></b></p>
        <?php endif;?>
    <?php endif;?>
</section>
<section>
    <br>
    <h3>Last 10 operation:</h3>
    <br>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">login</th>
                <th scope="col">E-mail</th>
                <th scope="col">Operation</th>
                <th scope="col">Current Value</th>
                <th scope="col">Final Value</th>
                <th scope="col">Date</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($vars['lastConverting'] as $task => $value): ?>
                <tr>
                    <td scope="row"><?php echo $value['login']; ?></td>
                    <td scope="row"><?php echo $value['email']; ?></td>
                    <td scope="row"><?php echo $value['operations']; ?></td>
                    <td scope="row"><?php echo $value['current_value']; ?></td>
                    <td scope="row"><?php echo $value['final_value']; ?></td>
                    <td scope="row"><?php echo $value['date']; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>

