<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title><?php echo $title; ?></title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Converter</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <?php if(isset($_SESSION['user'])): ?>
                    <li><a href="/account">Operation list</a></li>
                <?php endif;?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php if(!isset($_SESSION['name'])): ?>
                            Account
                        <?php else: ?>
                            <?php echo $_SESSION['name']; ?>
                        <?php endif; ?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php if(isset($_SESSION['name'])): ?>
                            <li><a href="/seting">Seting Currency Exchange</a></li>
                            <li><a href="/view">Seting Max Element</a></li>
                            <li><a href="/logout">logout</a></li>
                        <?php endif;?>
                        <?php if(!isset($_SESSION['name'])): ?>
                            <li><a href="/login">login</a></li>
                            <li><a href="/registration">Register</a></li>
                        <?php endif;?>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <?php echo $content;?>
</div>
<div class="footer">
    <hr>
    <p>&copy; 2019 <a href="https://www.facebook.com/profile.php?id=100004512672168&ref=bookmarks">vbudnik</a><p>
</div>
</body>
</html>
